function hide_toc() {

    const rightAsideWidth = "200px";

    const toggleSwitch = document.querySelector('.hide-toc input[type="checkbox"]');
    const currentToc = localStorage.getItem('toc-appearance');

    if (currentToc) {
        if (currentToc === 'disable') {
            $('.adoc-toc').css('transition','0s');
            document.getElementById('adoc-toc').style.width = "0px";
            $('#adoc-toc li').hide();
            $('#adoc-toc').hide();
            $('.adoc-toc').css('transition','0.3s');
            toggleSwitch.checked = true;    
        }
    }

    function switchTheme(e) {
        if (e.target.checked) {
            localStorage.setItem('toc-appearance', 'disable');
            $('.adoc-toc').css('transition','0.3s');  
            document.getElementById('adoc-toc').style.width = "0px";
            $('#adoc-toc li').hide();
        }
        else {        
            localStorage.setItem('toc-appearance', 'enable');
            $('.adoc-toc').css('transition','0.3s');
            $('#adoc-toc li').show();
            $('#adoc-toc').show();
            document.getElementById('adoc-toc').style.width = rightAsideWidth;
        }    
    }

    toggleSwitch.addEventListener('change', switchTheme, false);
}

hide_toc();