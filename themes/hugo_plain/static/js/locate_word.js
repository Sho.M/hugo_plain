
// Enable upper case / lower case agnostic jump
// makes new function named icontains: instead of contains:
jQuery.expr[':'].icontains = function(a, i, m) {
    return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
};

const locatePosition = function(element, queryString) {

    //複数文言があった時用
    var multiple = queryString.split(" ")
    var result
    var position = 99999  
    if (multiple.length === 1) {
        result = $(element + ":icontains(" + multiple[0] + ")")
    } else if (multiple.length === 2) {
        result = $(element + ":icontains(" + multiple[0] + 
                            "):icontains(" + multiple[1] + ")")
    } else if (multiple.length === 3) {
        result = $(element + ":icontains(" + multiple[0] + 
                            "):icontains(" + multiple[1] +
                            "):icontains(" + multiple[2] + ")")
    } else {
        position = 0
    }
    if( typeof result.offset() !== "undefined" ) {
        position = result.offset().top - 100;
    }

    return position
}



$(function() {

        
    var queryString = window.location.search.substring(2)

    if (queryString == null || typeof queryString === "undefined" || queryString === "") {
        return
    }

    queryString = decodeURI(queryString)

    var speed = 100;

    var positionP = locatePosition("p", queryString)
    var positionTitle = locatePosition(".title", queryString)
    var positionCaption = locatePosition("caption", queryString)
    var positionTh = locatePosition("th", queryString)
    var positionTd = locatePosition("td", queryString)
    var positionPre = locatePosition("pre", queryString)
    var positionAttribution = locatePosition(".attribution", queryString)
    var positionFootnote = locatePosition(".footnote", queryString)
    var positionH1 = locatePosition("h1", queryString)
    var positionH2 = locatePosition("h2", queryString)
    var positionH3 = locatePosition("h3", queryString)    
    var positionH4 = locatePosition("h4", queryString)    
    var positionH5 = locatePosition("h5", queryString)    
    var positionH6 = locatePosition("h6", queryString)    


    position = Math.min(
        positionP,
        positionTitle,
        positionCaption,
        positionPre,
        positionAttribution,
        positionFootnote,
        positionTh,
        positionTd,
        positionH1, 
        positionH2, 
        positionH3, 
        positionH4, 
        positionH5, 
        positionH6
    )


    $("html,body").animate({scrollTop:position}, speed);

    $('.mainofbase').mark(queryString)
    $('.adoc-toc').mark(queryString)


})