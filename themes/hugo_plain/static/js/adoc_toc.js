window.addEventListener('DOMContentLoaded', function () {

    var vartoc = document.getElementById('toc');
    var varbook = document.getElementById('adoc-toc');
    var vartocMobile = document.getElementById('toc-mobile');

    $('#toc').css('transition','0s');
    $('#toctitle').css('transition','0s');
    $('.sectlevel1').css('transition','0s');

    // 両方の要素がnullでない時
    if (vartoc !== null && varbook !== null) {
        varbook.innerHTML = vartoc.innerHTML; // adoc-tocにasciidocのtocを移植する。
        vartocMobile.innerHTML = varbook.innerHTML; // mobile向けのtocにもasciidocのtocを移植する。
        vartoc.innerHTML = ""; // content本文に出てくるTable of Contentsを削除する。
        $('#toc').remove(); //Table of Contentsのdivタグを消す。
        $('#toctitle').remove(); //"Table of Contents"の文言を消す。
        $('#toctitle').remove(); //"Table of Contents"の文言を消す。
    }
});