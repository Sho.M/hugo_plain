function dark() {
    const toggleSwitch = document.querySelector('.theme-switch input[type="checkbox"]');
    const currentTheme = localStorage.getItem('theme');

    if (currentTheme) {
        document.documentElement.setAttribute('data-theme', currentTheme);
    
        if (currentTheme === 'dark') {
            $('.mainofbase').css('transition','0s');
            $('.left-navigation').css('transition','0s');
            $('.horizontal-fold>#btn').css('transition','0s');
            $('.navLinks>li>a').css('transition','0s');
            $('.slider').css('transition','0s');
            $('.header').css('transition','0s');
            $('.adoc-toc').css('transition','0s');
        
            toggleSwitch.checked = true;

            $('.mainofbase').css('transition','0.3s');
            $('.left-navigation').css('transition','0.3s');
            $('.horizontal-fold>#btn').css('transition','0.3s');
            $('.navLinks>li>a').css('transition','0.3s');
            $('.slider').css('transition','0.3s');
            $('.header').css('transition','0.3s');
            $('.adoc-toc').css('transition','0.3s');
        
        }

    }

    function switchTheme(e) {
        if (e.target.checked) {
            document.documentElement.setAttribute('data-theme', 'dark');
            localStorage.setItem('theme', 'dark');
        }
        else {        
            document.documentElement.setAttribute('data-theme', 'light');
            localStorage.setItem('theme', 'light');
        }    
    }

    toggleSwitch.addEventListener('change', switchTheme, false);
}

dark();