// Acquire variables from css
//const leftNavWidth = $(":root").css("--left-nav-width");
//const buttonSize = $(":root").css("--btn-size");
//const transitionTime = $(":root").css("--transition-time");
//const rightAsideWidth = $(":root").css("--right-aside-width");

// As the above does not work for IE11, temporarily declaring variables here...
 const leftNavWidth = "200px";
 const buttonSize = "45px";
 const transitionTime = "0.3s";
 const rightAsideWidth = "200px";

// 左メニューの閉じた後の大きさ。ボタンとサイズを揃える。
const leftNavWidthClose = buttonSize; 

var menuStat = localStorage.getItem('menu-stat');

if (menuStat === "closed") {
    // 左メニューを閉じている状態で画面遷移時にアニメーションが発生しないようにtransitionを0sにする。
    $('.left-navigation').css('transition','0s');
    $('.mainofbase').css('transition','0s');
    $('#btn').css('transition','0s');
    // アニメーション無しで左メニューを閉じる。（画面を遷移した瞬間に既に閉じているようにする）
    $('#ul-menu').hide();
    $('.accordion').hide(); // 左メニューボタン非表示
    $('.sidenav').hide();
    //$('#left-navigation').css('width',leftNavWidthClose);
    document.getElementById('left-navigation').style.width = leftNavWidthClose;
    //$('#mainofbase').css('left',leftNavWidthClose);
    document.getElementById('mainofbase').style.left = leftNavWidthClose;
    //$('#mainofbase').css('width',"calc(100% - " + leftNavWidthClose + " - " + rightAsideWidth + " - 3px)");
    document.getElementById('mainofbase').style.width = "calc(100% - " + leftNavWidthClose + " - " + rightAsideWidth + " - 3px)";
    $('.fa-angle-double-left').addClass('fa-angle-double-right'); // ボタンのアイコン書き換え
    $('.fa-angle-double-right').removeClass('fa-angle-double-left');
    //$('#btn').css('width',leftNavWidthClose);
    document.getElementById('btn').style.width = leftNavWidthClose;
}


// ボタンクリック時の動作
$('.horizontal-fold').on('click', function() {
if (document.getElementById("btn").innerHTML == '<i class="fas fa-angle-double-left"></i>') {
    // 閉じている状態にcssの内容を書き換える。
    $('#ul-menu').hide(); //ulを非表示にする。
    //$('#left-navigation').css('width',leftNavWidthClose);
    document.getElementById('left-navigation').style.width = leftNavWidthClose;
    //$('#mainofbase').css('left',leftNavWidthClose);
    document.getElementById('mainofbase').style.left = leftNavWidthClose;
    //$('#mainofbase').css('width',"calc(100% - " + leftNavWidthClose + " - " + rightAsideWidth + " - 3px)");
    document.getElementById('mainofbase').style.width = "calc(100% - " + leftNavWidthClose + " - " + rightAsideWidth + " - 3px)";
    $('.fa-angle-double-left').addClass('fa-angle-double-right'); // ボタンのアイコン書き換え
    $('.fa-angle-double-right').removeClass('fa-angle-double-left');
    $('#btn').css('width',leftNavWidthClose);
    $('.accordion').hide(); // 左メニューボタン非表示
    $('.sidenav').hide();
    // メニューが閉じているという状況をlocalStorageに保存。
    localStorage.setItem('menu-stat', 'closed');
} else {
    // 最初から閉じている状態の時のためにアニメーション速度を戻す。
    $('.left-navigation').css('transition',transitionTime);
    $('.mainofbase').css('transition',transitionTime);
    $('#btn').css('transition',transitionTime);

    // 開いている状態にcssの内容を書き換える。
    //$('#left-navigation').css('width',leftNavWidth);
    document.getElementById('left-navigation').style.width = leftNavWidth;
    //$('#mainofbase').css('left',leftNavWidth);
    document.getElementById('mainofbase').style.left = leftNavWidth;
    //$('#mainofbase').css('width',"calc(100% - " + leftNavWidth + " - " + rightAsideWidth + " - 3px)");
    document.getElementById('mainofbase').style.width = "calc(100% - " + leftNavWidth + " - " + rightAsideWidth + " - 3px)";
    $('.fa-angle-double-right').addClass('fa-angle-double-left'); // ボタンのアイコン書き換え
    $('.fa-angle-double-left').removeClass('fa-angle-double-right');
    $('#ul-menu').show();  
    //$('#btn').css('width',leftNavWidth);
    document.getElementById('btn').style.width = leftNavWidth;
    $('.accordion').show(); // 左メニューボタン非表示
    $('.sidenav').show();
    // メニューを開いたらlocalStorageの情報を削除。
    localStorage.removeItem('menu-stat');
}
});