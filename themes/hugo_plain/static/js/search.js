/* This source code is the modified version of the following software under The MIT License:
https://github.com/koirand/pulp/blob/master/assets/js/search.js
*/

/* The MIT License (MIT)
Copyright (c) 2018 Kazuki Koide

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

https://github.com/koirand/pulp/blob/master/LICENSE.md
*/

var pagesIndex
var lunrIndex
var lunrResult

/**
 * A function for separating a string into bigram and join it with space.
 *
 * @static
 * @param {?string} query - The string to convert into tokens
 * @returns {string}
 */
const queryNgramSeparator = function (query) {
    const str = query.toString().trim().toLowerCase()
    const tokens = []

    for (let i = 0; i <= str.length - 2; i++) {
        tokens.push(str.slice(i, i + 2))
    }
    
    return tokens.join(' ')
}



/**
 * A function for splitting a string into bigram.
 *
 * @static
 * @param {?(string|object|object[])} obj - The object to convert into tokens
 * @param {?object} metadata - Optional metadata to associate with every token
 * @returns {lunr.Token[]}
 */
const bigramTokeniser = function (obj, metadata) {
    if (obj == null || obj === undefined) {
      return []
    }
  
    let str = obj.toString().trim().toLowerCase()
    let tokens = []
  
    for (let i = 0; i <= str.length - 2; i++) {
      const tokenMetadata = lunr.utils.clone(metadata) || {}
      tokenMetadata['position'] = [i, i + 2]
      tokenMetadata['index'] = tokens.length
      tokens.push(
        new lunr.Token(
          str.slice(i, i + 2),
          tokenMetadata
        )
      )
    }

    return tokens
}
  


function initLunr() {
    // Get {{ .Site.BaseURL }} of hugo variables from baseof.html
    const dataDiv = document.getElementById('site-base-url')
    $.getJSON(dataDiv.dataset.url + "index.json").done(function(index) {
        pagesIndex = index

        lunrIndex = lunr(function() {

            this.tokenizer = bigramTokeniser
            this.pipeline.reset()

            this.ref('ref')
            this.field('title', { boost: 10 })
            this.field('tags', { boost: 5 })
            this.field('body', { boost: 15 })
            this.metadataWhitelist = ['position']

            pagesIndex.forEach(function(page) {
                this.add(page)
            }, this)
        })
    })
}

function initUI() {

    $results = $("#searchResultsParent");

    //$("#searchBoxInput").keyup(function(event) {
    $("#searchBoxInput").on('input', function(event) {
        $("html,body").animate({scrollTop:0}, 100);
        $results.empty();
        const queryword = $(event.currentTarget).val()

        // Only trigger a search when 2 chars. at least have been provided
        if (queryword.length < 2) {
            $results.hide()
            $('#hide-for-search').show()
            return
        }

        // Switch search results and the contents shown before search
        $('#searchResultsParent').show()
        $('#hide-for-search').hide()

        var query = $(this).val()
        query = query.replace(/　/g, ' ') // Delete full-width space
        query = query.replace(/  /g, ' ') // Delete double space
        query = query.replace(/ *$/, '') // Delete space in the end 
        var results = search(query)
        
        renderResults(results)

        // If there is no query(searching text), show the contents again.
        if (query == "") {
            $('#hide-for-search').show()
            $('#searchResultsParent').hide()
        }    
    });

    // Show search results again when user come back from other pages.
    $('#searchBoxInput').trigger('keyup')
}

/**
 * Trigger a search in lunr and transform the result
 *
 * @param  {String} query
 * @return {Array}  results
 */
function search(query) {
    // Find the item in our index corresponding to the lunr one to have more info
    // Lunr result: 
    //  {ref: "/section/page1", score: 0.2725657778206127}
    // Our result:
    //  {title:"Page1", href:"/section/page1", ...}

    // console.log(lunrIndex.search(query))

    //mapはかっこのあとの処理を使って新しい配列を返している。

    //pagesIndexにfilterを使って新しい配列を作って、その0要素目を返している。

    // lunrIndexとpagesIndexの.refが同じ場合に
    // pagesIndexの0番目の要素を返している

    lunrResult = lunrIndex.search(queryNgramSeparator(query))

    return lunrResult.map(function(result) {
        return pagesIndex.filter(function(page) {
            return page.ref === result.ref;
        })[0];
    });
}




const locateQuery = function(separatedQueries, idx, whereToSearch) {

    var notFoundFlag = 0
    var keptPosition
    var matchedPosition = []

    separatedQueries.forEach( function(element1, index1) {
        // 初回は処理をせずにキープするだけ
        if ( index1 === 0 ) {
            if ( whereToSearch === "body" ) {
                if ( typeof lunrResult[idx].matchData.metadata[element1] === "undefined" || 
                    typeof lunrResult[idx].matchData.metadata[element1].body === "undefined" ) {
                    notFoundFlag = 1
                    return
                }
                keptPosition = lunrResult[idx].matchData.metadata[element1].body.position
            } else if ( whereToSearch === "title") {
                if ( typeof lunrResult[idx].matchData.metadata[element1] === "undefined" || 
                    typeof lunrResult[idx].matchData.metadata[element1].title === "undefined" ) {
                    notFoundFlag = 1
                    return
                }
                keptPosition = lunrResult[idx].matchData.metadata[element1].title.position
            }
        // 偶数回 or 奇数で且つ最後の場合だけ処理 
        } else if ( index1 % 2 === 0 || index1 === separatedQueries.length - 1 ) {
            if ( typeof keptPosition === "undefined") {
                return
            }
            keptPosition.forEach( function(element2, index2) {
                if ( whereToSearch === "body" ) {
                    if ( typeof lunrResult[idx].matchData.metadata[element1] === "undefined" || 
                        typeof lunrResult[idx].matchData.metadata[element1].body === "undefined" ) {
                        notFoundFlag = 1
                        return
                    }
                } else if ( whereToSearch === "title") {
                    if ( typeof lunrResult[idx].matchData.metadata[element1] === "undefined" || 
                        typeof lunrResult[idx].matchData.metadata[element1].title === "undefined") {
                        notFoundFlag = 1
                        return
                    }
                }


                if ( whereToSearch === "body" ) {                
                    lunrResult[idx].matchData.metadata[element1].body.position.forEach( function(element3, index3) {
                        if ( index1 % 2 === 0 ) {
                            if (element2[0] + 2 === element3[0]) {
                                var array = [ element3[0] , element3[0] + 2 ]
                                matchedPosition.push(array)
                            }
                        } else if (index1 === separatedQueries.length - 1) {
                            if (element2[0] + 1 === element3[0]) {
                                var array = [ element3[0] , element3[0] + 1 ]
                                matchedPosition.push(array)
                            }
                        }
                    })
                } else if ( whereToSearch === "title") {
                    lunrResult[idx].matchData.metadata[element1].title.position.forEach( function(element3, index3) {
                        if ( index1 % 2 === 0 ) {
                            if (element2[0] + 2 === element3[0]) {
                                var array = [ element3[0] , element3[0] + 2 ]
                                matchedPosition.push(array)
                            }
                        } else if (index1 === separatedQueries.length - 1) {
                            if (element2[0] + 1 === element3[0]) {
                                var array = [ element3[0] , element3[0] + 1 ]
                                matchedPosition.push(array)
                            }
                        }
                    })
                }
            })
            if (matchedPosition.length !== 0) {
                keptPosition = matchedPosition
                matchedPosition = []
            } else {
                notFoundFlag = 1
                return
            } 
        } 
    })

    var positions = new Object()
    positions.notFoundFlag = notFoundFlag
    positions.keptPosition = keptPosition

    return positions
}


/**
 * Rendering search results
 * @param {Object[]} results Array of search results
 */
const renderResults = function(results) {
    const $searchResultsParent = $('#searchResultsParent')
    const searchResultsParent = document.getElementById("searchResultsParent");
  
    var query = $('#searchBoxInput').val()
    query = query.replace(/　/g, ' ') // Delete full-width space
    query = query.replace(/  /g, ' ') // Delete double space
    query = query.replace(/ *$/, '') // Delete space in the end 
    const BODY_LENGTH = 100

    // Clear search result
    $searchResultsParent.empty()
  
    // Show message when results is empty
    if (!results.length) {

        searchResultsParent.innerHTML = '<div class="notFound"> No results found for query : ' + query + '</div>'
  
        return
    }   

    
    var searchResultsHeader = document.createElement("h4")
    searchResultsHeader.innerHTML = 'Search Results'
    searchResultsParent.appendChild(searchResultsHeader)

    var loops = 0
    var foundFlag = 0
    var foundFlagTitle = 0

    
    // Only show the ten first results
    results.forEach(function(result, idx) {

        const metadata = lunrResult[idx].matchData.metadata

        // 単語ごと（ユーザが分割して入力した単語ごと）にseparatedQueriesが配列の要素として別々にできる。
        var separatedQueries = []
        query.split(' ').forEach(function(eachQuery) {
            separatedQueries.push(queryNgramSeparator(eachQuery).split(' '))
        })

        // それぞれの単語のポジションを取得してpositionsBodyの配列の要素として格納する。
        var positionsBody = []
        var positionsTitle = []
        separatedQueries.forEach(function(eachSeparatedQueries) {
            positionsBody.push(locateQuery(eachSeparatedQueries, idx, "body"))
            positionsTitle.push(locateQuery(eachSeparatedQueries, idx, "title"))
        })

        var keptPosition = []
        var notFoundFlag = []
        positionsBody.forEach(function(eachPositionsBody) {
            keptPosition.push(eachPositionsBody.keptPosition)
            notFoundFlag.push(eachPositionsBody.notFoundFlag)
        })
        
        var notFoundFlagTitle = []
        positionsTitle.forEach(function(eachPositionsTitle) {
            notFoundFlagTitle.push(eachPositionsTitle.notFoundFlag)
        })

        //var keptPosition = positionsBody.keptPosition
        //var notFoundFlag = positionsBody.notFoundFlag

        //console.log(keptPosition)

        //var positionsTitle = locateQuery(separatedQueries, idx, "title")

        //var notFoundFlagTitle = positionsTitle.notFoundFlag

        var notFoundFlagResult = 0
        notFoundFlag.forEach(function(flag){
            if (flag === 1) {
                notFoundFlagResult = 1
            }
        })

        var notFoundFlagTitleResult = 0
        notFoundFlagTitle.forEach(function(flag){
            if (flag === 1) {
                notFoundFlagTitleResult = 1
            }
        })

        if ( notFoundFlagResult === 0 ) {
            foundFlag = 1
        } 

        if ( notFoundFlagTitleResult === 0 ) {
            foundFlagTitle = 1
        } 

        loops++

        // Show message when results is empty
        if ( foundFlag === 0 && foundFlagTitle === 0 && results.length === loops ) {
            searchResultsParent.innerHTML = '<div class="notFound"> No results found for query : ' + query + '</div>'
            return
        }


        var finalPositions = []
        var updatedFinalPositions = []
        keptPosition.forEach( function(eachKeptPosition, idx) {
            
            if (typeof eachKeptPosition === "undefined") {
                return
            }

            // 初回は何もしない
            if ( idx === 0 ) {
                finalPositions = eachKeptPosition

                var loopnum = finalPositions.length

            } else {
                eachKeptPosition.forEach( function(eachPosition) {

                    for (let i = 0; i < loopnum; i++) {
                        if ( Math.abs(finalPositions[i][0] - eachPosition[0]) < 50 ) {
                            // 元のpositionのフォーマットと合わせるため0をつけて二次元配列にする
                            updatedFinalPositions.push([finalPositions[i][0], 0])
                        }
                    }
                })
                finalPositions = updatedFinalPositions
            }
        })

        if (updatedFinalPositions.length === 0) {
            updatedFinalPositions = keptPosition[0]
        }

        var matchPosition 
        if ( notFoundFlagResult === 1 ) {
            matchPosition = 0
        } else {
            matchPosition = updatedFinalPositions[0][0]
        }






        if ( notFoundFlagResult === 1 && notFoundFlagTitleResult === 1) {
            return
        }

        const bodyStartPosition = (matchPosition - (BODY_LENGTH / 2) > 0) ? matchPosition - (BODY_LENGTH / 2) : 0

        var searchResultChildTitle = document.createElement("div");
        searchResultChildTitle.class = "searchResultChildTitle"; 
        var searchResultChildBody = document.createElement("div");
        searchResultChildBody.class = "searchResultChildBody"; 

        var encodedQuery = encodeURI(query)

        var url = result.ref + "?=" + encodedQuery

        searchResultChildTitle.innerHTML = '<a class="searchResultChildTitle" href="' + url + '">' + result.title + '</a>'
        searchResultsParent.appendChild(searchResultChildTitle)

        searchResultChildBody.innerHTML = '<div class="searchResultChildBody">' + result.body.substr(bodyStartPosition, BODY_LENGTH) + '</div>' 
        searchResultsParent.appendChild(searchResultChildBody)

        // Highlight keyword
        $('#searchResultsParent').mark(query)
    })
}
  
// Let's get started
initLunr()

$(document).ready(function() {
    initUI();
});