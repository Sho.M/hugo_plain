$(document).ready(function() { 

    // make table element "tablesorter" when there is "declare-tablesorter" class DOM before the table.
    $(".declare-tablesorter").parent().each(function(i, elem) {
        $(elem).nextAll("table:first").addClass("tablesorter");
    });
    // put Rest button above the table.
    $(".tablesorter").before('<button class="reset-button">Reset</button>');
    // make th element of the table works for tablesorter.
    $("th").attr("data-placeholder","Filter...");

    $(".tablesorter").tablesorter({
        theme: 'blue',
        widthFixed : true,
        widgets: ['filter','zebra'],
        widgetOptions : {
            filter_hideEmpty : true,
            filter_hideFilters : true,
        }      
    });

    $(".reset-button").click(function() {
        $("table").trigger("filterReset").trigger("sortReset");
        return false;
    });
});
