---
title: "最初のルール"
date: 2020-04-30T23:37:51+09:00
menu: "procedure"
categories:
 - ルール
tags: 
 - 一つ目の記事
weight: 1
outputs:
- html
---

:toc:
:sectnums:
:imagesdir: ../../img
:includedir: _includes

|===
|hoge
|===


## 最初のルールです。
### ルールの中身について。

ルールの中身についての記述。

### 表の記述例

.表の例その1
|===
|英語|英語
|英語|英語
|===

.表の例その2
[options="header,footer"]
|=================================
|カラム 1|カラム 2      |カラム 3
|1       |アイテム 1    |a
|2       |アイテム 2    |b
|3       |アイテム 3    |c
|合計6   |Three items   |d
|=================================

.表の例その3
|====
|Date |Duration |Avg HR |Notes

|22-Aug-08 .2+^.^|10:24 | 157 |
Worked out MSHR (max sustainable
heart rate) by going hard
for this interval.

|22-Aug-08 | 152 |
Back-to-back with previous interval.

|24-Aug-08 3+^|none
|====