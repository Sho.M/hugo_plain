---
title: "図形についてのガイドですよおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおおお"
date: 2020-04-30T23:37:51+09:00
menu: "guide"
categories:
 - ガイド
tags:
 - 一つ目の記事
weight: 1
outputs:
- html
---

:toc:
:sectnums:
:sectlinks:
:imagesdir: ../../img
:includedir: _includes

## 図形

### Graphviz

link:http://localhost:1313/hugo_plain/feature/guide/table_guide/#_table_inside[ほげほげ]
ですです。

[graphviz, "diagram04", "svg"]
----
digraph G { rankdir=LR; Graphviz->AsciiDoc->HTML}
----

[graphviz, "diagram03", "svg"]
----
digraph G { rankdir=LR; 日本語->英語->スペイン語}
----

[graphviz, diagramx-1, "svg"]
----
digraph G { rankdir=LR; 日本語1->英語->スペイン語}
----

[graphviz, diagramx-2, svg]
----
digraph G { rankdir=LR; 日本語2->英語->スペイン語}
----

[graphviz, "diagramx-3", svg]
----
digraph G { rankdir=LR; 日本語3->英語->スペイン語}
----


### Ditaa

[ditaa, "diagram01", "svg"]
----
+-------+     +-------+
|  Plan |     |  Do   |
|  計画   +---->+  実行   |
+-------+     +-------+
----

Ditaaを書いてみるぞ

[ditaa, "diagram02", "svg"]
----
                   +-------------+
                   | Asciidoctor |-------+
                   |   diagram   |       |
                   +-------------+       | PNG out
                       ^                 |
                       | ditaa in        |
                       |                 v
 +--------+   +--------+----+    /---------------\
 |        | --+ Asciidoctor +--> |               |
 |  Text  |   +-------------+    |   abc         |
 |Document|   |   !magic!   |    |    日本語        |
 |     {d}|   |             |    |               |
 +---+----+   +-------------+    \---------------/
     :                                   ^
     |          Lots of work             |
     +-----------------------------------+

abc=HOGEHO

----

Plantumlを書いてみるぞ

[plantuml, "plantuml-diagram", "svg"]
----
class Animal {
  run()
}

class "猫 " extends Animal {
}
----

### テスト

ABCDEAFAD

### ほげほげ

ABCDEAFAD

### mogemoge

ABCDEAFAD

### てててってててて

ABCDEAFAD