#!/bin/bash

CURRENT_DIR=`pwd`
WORK_DIR=$1

cd ${WORK_DIR}

# For each .adoc file in current dir.
for FILE in *.adoc ; do

	i=0
	DASH_COUNT=0
	SAVED_LINE=0

	while read LINE ; do

		if [ "${LINE}" = "---" ]; then
			if [ ${DASH_COUNT} = 1 ]; then
				SAVED_LINE=${i}
				break
			else 
				DASH_COUNT=1
			fi	
		fi
	
		i=$(( ${i} + 1 ))
	
	done < ${FILE}

	SAVED_LINE=$(( ${SAVED_LINE} + 1))
	sed '1,'${SAVED_LINE}'d' ${FILE} > del_dash_${FILE}
	sed -e 's/<div class="declare-tablesorter"><\/div>//g' del_dash_${FILE} > del_dash2_${FILE}
	sed -e 's/+++//g' del_dash2_${FILE} > mod_${FILE}
	rm -f del_dash_${FILE} del_dash2_${FILE} 

	echo "org FILE: ${FILE}, out FILE: mod_${FILE}, SAVED_LINE: ${SAVED_LINE}"

done

cd ${CURRENT_DIR}